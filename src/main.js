import Vue from 'nativescript-vue'
import App from './components/App'
import { firebase } from '@nativescript/firebase'

import RadChart from 'nativescript-ui-chart/vue';
import RadSideDrawer from 'nativescript-ui-sidedrawer/vue'
import RadListView from 'nativescript-ui-listview/vue'
import TabsPlugin from '@nativescript-community/ui-material-tabs/vue';


Vue.use(TabsPlugin);
Vue.use(RadSideDrawer)
Vue.use(RadListView)
Vue.use(RadChart)
Vue.registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager"). PreviousNextView)




new Vue({
  render: h => h('frame', [h(App)])
}).$start()

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

// Firebase Init
firebase
  .init({
    persist:false
  })
  .then(
    function(instance) {
      // console.log("firebase.init done");
    },
    function(error) {
      console.log("firebase.init error: " + error);
    }
);