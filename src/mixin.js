import { firebase } from '@nativescript/firebase'
export default {
  methods: {
    // Firebase
    getDashboardRef(dashboardId) {
      return firebase.firestore.collection("dashboards").doc(dashboardId)
    },
    getUserRef(dashboardId, currentUserId){
      return firebase.firestore.collection("dashboards").doc(dashboardId).collection("users").doc(currentUserId)
    },
    getDashboardTasks(dashboardId){
      return firebase.firestore.collection("dashboards").doc(dashboardId).collection("selectedTasks")
    },
    getDashboardUsers(dashboardId){
      return firebase.firestore.collection("dashboards").doc(dashboardId).collection("users")
    },
    getColors(){
      return firebase.firestore.collection("colors").doc("colors")
    },
    
    hexToRGB(hex, alpha) {
      var r = parseInt(hex.slice(1, 3), 16),
          g = parseInt(hex.slice(3, 5), 16),
          b = parseInt(hex.slice(5, 7), 16);
      if (alpha) {
          return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
      } else {
          return "rgb(" + r + ", " + g + ", " + b + ")";
      }
    },

    convertMs(milliseconds) {
      let arrayOfWeekdays = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]
      let day = new Date(milliseconds).getDay()
      let hour = new Date(milliseconds).getHours()
      let minute = new Date(milliseconds).getMinutes()
      if (minute < 10) {
        minute = '0' + minute
      }
      return {
        hours: hour + ':' + minute,
        day: arrayOfWeekdays[day]
      }
    },

    validateForm(value, users = []){
      let result = {
        valid:true,
        error:""
      }
      if (value && users.length > 0 && users.some(user => user.name === value)) {
        result.valid = false
        result.error = "L'utilisateur " + value + " existe déjà, veuillez choisir un autre nom"
      }
      if (!value) {
        result.valid = false
        result.error = "Le champ ne peut pas être vide"
      }
      if (value.length > 12) {
        result.valid = false
        result.error = "12 caractères maximum"
      }
      return result
    }
  }
}